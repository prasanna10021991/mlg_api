const userModel = require('../models/userModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = {
  create: function (req, res, next) {
    userModel.findOne({ email: req.body.email }, function (err, userInfo) {
      if (err) {
        next(err);
      } else {
        if (userInfo) {
          res.json({ status: "false", message: "Email already available!!!", data: null });
        }
        else {
          userModel.create({ email: req.body.email, lastName: req.body.lastName, firstName: req.body.firstName, password: req.body.password, role: req.body.role }, function (err, result) {
            if (err)
              next(err);
            else
              res.json({ status: "success", message: "User added successfully!!!", data: null });
          });
        }
      }
    });
  },


  addUser: function (req, res, next) {
    userModel.findOne({ email: req.body.email }, function (err, userInfo) {
      if (err) {
        next(err);
      } else {
        if (userInfo) {
          res.json({ status: "false", message: "Email already available!!!", data: null });

        }
        else {
          userModel.create({ email: req.body.email, firstName: req.body.firstName, lastName: req.body.lastName, password: req.body.password, role: req.body.role }, function (err, result) {
            if (err)
              next(err);
            else
              res.json({ status: "success", message: "User added successfully!!!", data: null });
          });
        }
      }
    });
  },


  authenticate: function (req, res, next) {
    console.log(req.body);
    userModel.findOne({ email: req.body.email }, function (err, userInfo) {
      if (err) {
        next(err);
      } else {
        if (userInfo) {
          if (bcrypt.compareSync(req.body.password, userInfo.password)) {
            const token = jwt.sign({ id: userInfo._id }, req.app.get('secretKey'), { expiresIn: '1h' });
            res.json({ status: "success", message: "user found!!!", data: { user: userInfo, token: token } });
          } else {
            res.json({ status: "error", message: "Invalid email/password!!!", data: null });
          }
        }
        else {
          res.json({ status: "error", message: "Wrong email!!!", data: null });
        }
      }
    });
  },

  getAll: function (req, res, next) {
    let userList = [];
    userModel.find({}, function (err, users) {
      if (err) {
        next(err);
      } else {
        for (let user of users) {
          userList.push({ user });
        }
        res.json({ status: "success", message: "user list found!!!", data: { users: userList } });
      }
    });

  },


  updateById: function (req, res, next) {
    userModel.findByIdAndUpdate({ _id: req.params.id }, { name: req.body.name }, function (err, userInfo) {
      if (err)
        next(err);
      else {
        res.json({ status: "success", message: "user data updated successfully!!!", data: null });
      }
    });
  },


  deleteById: function (req, res, next) {
    userModel.findByIdAndRemove({ _id: req.params.id }, function (err, userInfo) {
      if (err)
        next(err);
      else {
        res.json({ status: "success", message: "user deleted successfully!!!", data: null });
      }
    });
  },


}