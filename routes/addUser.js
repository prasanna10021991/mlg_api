const express = require('express');
const router = express.Router();
const userController = require('../app/api/controllers/userController');
router.post('/adduser', userController.addUser);
router.get('/getAll', userController.getAll);
router.delete('/delete', userController.deleteById);
module.exports = router;