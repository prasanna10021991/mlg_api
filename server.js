const express = require('express');
const logger = require('morgan');
var cors = require('cors')
var sys = require('sys')
var exec = require('child_process').exec;
const PORT = process.env.PORT||5000;
const fs = require('fs');
exec('command', function (error, stdout, stderr) { });

const users = require('./routes/users');
const userData = require('./routes/adduser');
const bodyParser = require('body-parser');


//const mongoose = require('./config/database'); //database configuration
var path = require('path');
var jwt = require('jsonwebtoken');
const app = express();

app.set('secretKey', 'nodeRestApi'); // jwt secret token

// connection to mongodb
//mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(cors());
// create a write stream (in append mode)
var logStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
app.use(logger('dev', { stream: logStream }));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())


app.get('/', function (req, res) {
    res.json({ "data": "working fine" });
});



// public route
app.use('/users', users);
app.use('/userData', validateUser, userData);



function validateUser(req, res, next) {
    jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, data) {
        if (err) {
            res.json({ status: "error", message: err.message, data: null });
        } else {
            // add user id to request
            req.body.userId = data.id;
            next();
        }
    });
}

// express - handle 404 error
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// handle errors
app.use(function (err, req, res, next) {
    console.log(err);

    if (err.status === 404)
        res.status(404).json({ message: "Not found" });
    else
        res.status(500).json({ message: "Something looks wrong :( !!!" });
});


app.listen(PORT, function () {
    console.log(`Node server listening on port ${PORT}`);
});